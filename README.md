Anya's Army 
=========

Website I created for Anya's Army Fundraiser

<strong>Things to be fixed:</strong>
<ul>
<li>PHP so contact form POST emails-405 error</li>
<li>Pledge form that stores information and links to prefilled FB</li> 
<li>Fix Facebook error about hostname not resolving to IP</li> 
<li>Needs two mission statements</li> 
<li>Needs text on Pictures page</li>
<li>Most of Help page needs info</li>
<li>Add message for submit form so user knows submitted</li>
<li>Get widget code for GoFundMe</li>
</ul>
